package com.karima.retrofit

interface ProductResponse<T> {

    fun success(response: T)
}