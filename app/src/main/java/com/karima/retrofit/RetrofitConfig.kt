package com.karima.retrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitConfig {


      val retrofit =  Retrofit.Builder()
            .baseUrl("http://192.168.43.210:3000")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    fun productService(): ProductService = retrofit.create(ProductService:: class.java)

}