package com.karima.retrofit

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductWebClient {

    fun list(productResponse: ProductResponse<ArrayList<Product>>) {
        val call = RetrofitConfig().productService().list()
        call.enqueue(object : retrofit2.Callback<ArrayList<Product>> {
            override fun onFailure(call: Call<ArrayList<Product>>, t: Throwable) {
                print(t)
            }

            override fun onResponse(
                call: Call<ArrayList<Product>>,
                response: Response<ArrayList<Product>>
            ) {
                response.body()?.let {
                    productResponse.success(it)
                }


            }
        })
    }

    fun insert(product: Product, productResponse: ProductResponse<Product>) {
        val call = RetrofitConfig().productService().insert(product)
        call.enqueue(object : Callback<Product> {
            override fun onFailure(call: Call<Product>, t: Throwable) {
                print(t)
            }

            override fun onResponse(call: Call<Product>, response: Response<Product>) {
                response.body()?.let {
                    productResponse.success(it)
                }

            }

        })

    }

    fun alter(product: Product, callResponse: (products: Product?, throwable: Throwable?) -> Unit) {
        val call = product.id?.let { RetrofitConfig().productService().alter(product, it) }
        call?.enqueue(object : Callback<Product> {
            override fun onFailure(call: Call<Product>, t: Throwable) {
            }

            override fun onResponse(call: Call<Product>, response: Response<Product>) {
                response.body()?.let {
                    callResponse(it, null)
                }
            }

        })
    }

    fun delete(id: String, callResponse: (response: String?, throwable: Throwable?) -> Unit) {
        val call = RetrofitConfig().productService().delete(id)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                response.body()?.let {
                    callResponse(it.string(), null)
                }
            }

        })
    }
}